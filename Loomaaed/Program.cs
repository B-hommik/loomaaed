﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loomaaed
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom siga = new Loom("Metssiga");
            Console.WriteLine(siga);
            siga.TeeH22lt();

            Koduloom koduSiga = new Koduloom("Kodusiga");
            koduSiga.Nimi = "Nosu";
            Console.WriteLine(koduSiga);
            koduSiga.TeeH22lt();

            Koer k1 = new Koer("S2ssi");
            k1.TeeH22lt();

            Kass m1 = new Kass("Kurru");
            m1.TeeH22lt();

            List<Loom> loomad = new List<Loom>(); //nii Loom kui Koduloom saavad ühte listi minna, kuna koduloom on copy-paste loomade klassist. Vastupidi ei saa teha, kuna Loom on eraldi klass, ei ole tuletatud.
            loomad.Add(siga);
            loomad.Add(koduSiga);
            foreach (var x in loomad)
            { Console.WriteLine(x); } //trükitakse välja mõlemad loomad, aga oma To stringi lausega
        }
    }
    class Loom
    {
        public string Liik;
        public Loom(string liik= "tundmatu") { Liik = liik; }//konsktruktor, paneb vaikimisi loomale liigi
        public virtual void TeeH22lt() => Console.WriteLine($"{Liik} teeb koledat häält"); //kui ma siia virtual kirjutan, siis saan järgmises klassis see üle kirjutada/ defineerida
        public override string ToString() => $"on loom liigist {Liik}";
    }
    class Koduloom : Loom // Koduloom on tuletatud klass ja Loom on baasklass. Konstruktorid kaasa ei tule, funktsioonid ja väljad tulevad.
    {
        public string Nimi;
        //public Koduloom(string liik, string nimi = "nimepole") { Liik = liik; Nimi = nimi; }
        public Koduloom(string liik, string nimi = "nimepole") : base(liik)//saad kasutada baasklassi konstruktorit
        { Nimi = nimi; }
        public override string ToString() => $"{Nimi} on {Liik}";// see on selleks et me siia oma baasklassi liiki ei saaks, kuna kodulooma liik on teistlaadi
        public override void TeeH22lt() //selleks et seda üle kirjutada tuleb eelmsies klassis kirjutada virtual
        {
            Console.WriteLine($"{Nimi} teeb mahedat häält");
        }
    }
        class Koer : Koduloom
        {
            public string Tõug = "krants";
            public Koer (string nimi) : base("koer", nimi) { }//koduloomal oli ette öeldud mis parameetrid tal on, seega pidime tegema liigi ümbernimetamise konstruktori, liigiks läks koer
            public override void TeeH22lt()
            {
                Console.WriteLine($"{Nimi} teeb auh-auh");
            }
        }
        class Kass : Koduloom
        {
            public string Tõug = "segavereline";
            public Kass(string nimi) : base("kass", nimi) { }//koduloomal oli ette öeldud mis parameetrid tal on, seega pidime tegema liigi ümbernimetamise konstruktori, liigiks läks kass
        public override void TeeH22lt()
        {
            Console.WriteLine($"{Nimi} teeb mjäu-mjäu");
        }
    }
    }

